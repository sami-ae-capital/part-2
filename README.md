Challenge: 
Write a build pipeline script (preferably using Jenkins) that performs the following tasks:
- Create and build an empty .NET webapp
- Create a docker container for the webapp
- Provision an EC2 instance and deploy the container as a service running on ECS
- Any parameters required, such as AWS region or account number should be retrieved from a
configuration file.

# AE Capital Web App and Infrastructure

This repository contains the source code for a dotnet web application as well as the infrastructure code to containerise and host it. There are also two pipeline files here, one for Gitlab and one for Jenkins.

This project is deployed here: https://aecapital.fatherbean.com
     
Note: this may be taken offline for cost reasons. If it is offline at the time of review but you want it live please email sami@fatherbean.com

## Pipelines

As I was using Gitlab as the git store for the code I decided to also use Gitlab's in-built Terraform state storage. This means we can run a .gitlab-ci.yml pipeline and have our Terraform state managed automatically. For this reason I've also added a working Gitlab pipeline (see `.disabled-gitlab-cd.yml`).

The Jenkins pipeline connects to the Gitlab state storage for the infrastructure changes.

## Prerequisits

**Route53 Hosted Zone**

This project requires a hosted zone to be setup prior to the pipeline's being run. This ensures DNS resolution and certificate generation can be run successfuly. 

**Store IAM user credentials as Jenkins credentials**

An IAM user is needed to interact with AWS. If you want to generate a new IAM user and credentials run the `./create-iam-user.sh` script.

Upload the following variables as Jenkins credentials:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

## Getting Started

**Provision Core Infrastructure**

You must first provision the core infrastructure and generate the ECR repositories for your applications.

1. Set the `AWS_REGION` variable in the `config.properties` file
2. Add the following secrets as Jenkins credentials
      - AWS_ACCESS_KEY_ID
      - AWS_SECRET_ACCESS_KEY
      - Repository SSH key
      - Gitlab access token (for Terraform state storage)
2. Create a new Jenkins job that reference the `Jenkinsfile.infrastructure` file
3. Run the infrastructure job to provision the AWS infrastructure
4. Copy the image_repo_url output and add it into the `config.properties` file

**Deploy the Application**

1. Create a new Jenkins job that reference the `Jenkinsfile` file
2. Run the job to build and deploy the docker container to ECS

Optional: Setup a Jenkins trigger to listen to changes on the `main` branch. Once done this pipeline is fully CI and CD.


### Gitlab (Additional Pipeline)

This project is fully also fully running in Gitlab pipelines. The setup is very much the same as Jenkins.

**Provision Core Infrastructure**

You must first provision the core infrastructure and generate the ECR repositories for your applications.

1. Set the `AWS_REGION` variable in the `config.properties` file
2. Add the following secrets as Gitlab CI/CD credentials
      - AWS_ACCESS_KEY_ID
      - AWS_SECRET_ACCESS_KEY
3. Make a change in the `infrastructure` directory (to ensure the pipeline will build the provision-infrastructure job)
4. Commit and push
5. Copy the image_repo_url output and add it into the `config.properties` file

**Deploy Applications**

1. Commit and push a change or run the pipeline manually in Gitlab

The application will now be built on each commit to the repository enabling both CI and CD.

## Review Notes

I've setup a Jenkins deployment on Kubernetes to test the pipelines. Builds run successfully tagging the docker image with the latest Git SHA.

## Limitations and Pitfalls

**Image Deployment Using Terraform**

Some might argue that a deployment of an application should not be done using Terraform as it should only be used for infrastructure changes, which I somewhat agree with.

Because the ECS services and task definitions are created using Terraform it makes sense in this project to use Terraform as the deployment method. As modifying the task definitions in another way would cause the Terraform state to become out of sync.

We might be able to impliment a tool like [ecs-deploy](https://github.com/silinternational/ecs-deploy) to handle changes to task definitions, though the Terraform state will still need to be updated.

**Terraform Isolation**

This project contains all of the infrastructure files used to deploy the entire solution. This includes core infrastructure like the VPC, subnets, and more. This can introduce a security risk as developers that may not have sufficient knowledge about AWS infrastructure might change the files and cause the entire system to go down.

As a remedy to this I would move the core infrastructure into a different repository and have it provisioned there. Then the application specific infrastructure can remain in this project and reference the Terraform state of the core infrastructure. This would ensure that changes in this repository would be isolated to only the application. 

**Manual Intervention**

Currently there is a manual step needed to copy the ECR registries into the config file. Because we require the registries to be created before pushing Docker images this needs to happen in it's own step.

This could be automated by taking the output from the Terraform provision job and committing it to the config file automatically. Since it is a one off task I believe it is fine to leave for now.