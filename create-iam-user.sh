read -p "Enter the project name (default: aecapital): " PROJECT_NAME
PROJECT_NAME=${PROJECT_NAME:-aecapital}
read -p "Enter aws region (default: ap-southeast-2): " AWS_REGION
AWS_REGION=${AWS_REGION:-ap-southeast-2}
read -p "Enter aws profile (default: default): " AWS_PROFILE
AWS_PROFILE=${AWS_PROFILE:-default}

USER_CHECK=$(aws iam list-user-policies \
    --user-name $PROJECT_NAME \
    --profile $AWS_PROFILE \
    2>/dev/null)

if [ -n "$USER_CHECK" ]; then
    echo "User with name $PROJECT_NAME already exists, exiting."
    exit 1
fi

echo "Working."

aws iam create-user \
    --user-name $PROJECT_NAME \
    --profile $AWS_PROFILE \
    1>/dev/null

aws iam attach-user-policy \
    --user-name $PROJECT_NAME \
    --policy-arn arn:aws:iam::aws:policy/PowerUserAccess \
    --profile $AWS_PROFILE

aws iam attach-user-policy \
    --user-name $PROJECT_NAME \
    --policy-arn arn:aws:iam::aws:policy/IAMFullAccess \
    --profile $AWS_PROFILE

key=$(aws iam create-access-key \
    --user-name $PROJECT_NAME \
    --query 'AccessKey.{AccessKeyId:AccessKeyId,SecretAccessKey:SecretAccessKey}' \
    --profile $AWS_PROFILE \
    2>/dev/null)

AWS_ACCESS_KEY_ID=$(echo "$key" | jq '.AccessKeyId' --raw-output)
AWS_SECRET_ACCESS_KEY=$(echo "$key" | jq '.SecretAccessKey' --raw-output)

echo "IAM user created."
echo "AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY"
