// This Jenkinsfile is to be run once for the initial infrastructure setup.
// After completion add the ECR output from the terraform command into the config.properties file

podTemplate(
    label: 'build',
    containers: [
        containerTemplate(name: 'terraform', image: 'hashicorp/terraform:latest', command: 'sleep', args: '99d'),
    ]
) {
    node('build') {

        stage('Checkout') {
            git credentialsId: 'jenkins-gitlab', url: 'git@gitlab.com:ae-capital/dotnet-ecs.git', branch: 'main'
        }

        stage('Provision Infrastructure') {
            container('terraform') {
                withCredentials([
                    string(credentialsId: 'gitlab-access-token', variable: 'TOKEN'),
                    string(credentialsId: 'AWS_ACCESS_KEY_ID', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'AWS_SECRET_ACCESS_KEY', variable: 'AWS_SECRET_ACCESS_KEY'),
                ]) {
                    sh '''
                        cd infrastructure

                        export TF_VAR_AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
                        export TF_VAR_AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

                        # Setup the remote backend to our Gitlab project
                        export GITLAB_ACCESS_TOKEN=$TOKEN
                        terraform init \
                            -backend-config="address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp" \
                            -backend-config="lock_address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp/lock" \
                            -backend-config="unlock_address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp/lock" \
                            -backend-config="username=beanah" \
                            -backend-config="password=$GITLAB_ACCESS_TOKEN" \
                            -backend-config="lock_method=POST" \
                            -backend-config="unlock_method=DELETE" \
                            -backend-config="retry_wait_min=5"

                        terraform plan
                        terraform apply -auto-approve
                    '''
                }
            }
        }
    }
}