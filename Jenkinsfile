podTemplate(
    label: 'build',
    containers: [
        containerTemplate(name: 'terraform', image: 'hashicorp/terraform:latest', command: 'sleep', args: '99d'),
        containerTemplate(name: 'docker-aws-cli', image: 'amazon/aws-cli', command: 'sleep', args: '99d'),
    ],
    volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
    ]
) {
    node('build') {

        def GIT_COMMIT_HASH
        stage('Checkout') {
            git credentialsId: 'jenkins-gitlab', url: 'git@gitlab.com:ae-capital/dotnet-ecs.git', branch: 'main'
            GIT_COMMIT_HASH = sh (script: "git rev-parse HEAD | awk '{print substr(\$0,0,5)}' | tr -d '\n'", returnStdout: true)
        }

        stage('Test image') {
            sh "Add tests here"
        }

        stage('Build Docker image') {
            container('docker-aws-cli') {
                withCredentials([
                    string(credentialsId: 'AWS_ACCESS_KEY_ID', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'AWS_SECRET_ACCESS_KEY', variable: 'AWS_SECRET_ACCESS_KEY'),
                ]) {
                    sh """
                        amazon-linux-extras install docker                    
                        export \$(xargs < config.properties) # Collect the repository info from the config.properties file
                        cd webapp
                        docker build --tag \$imageRegistry/\$imageRepository:"${GIT_COMMIT_HASH}" --tag \$imageRegistry/\$imageRepository:latest .
                        aws ecr get-login-password | docker login --username AWS --password-stdin \$imageRegistry
                        docker push \$imageRegistry/\$imageRepository:"${GIT_COMMIT_HASH}"
                        docker push \$imageRegistry/\$imageRepository:latest
                    """
                }
            }
        }

        stage('Deploy image') {
            container('terraform') {
                withCredentials([
                    string(credentialsId: 'gitlab-access-token', variable: 'TOKEN'),
                    string(credentialsId: 'AWS_ACCESS_KEY_ID', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'AWS_SECRET_ACCESS_KEY', variable: 'AWS_SECRET_ACCESS_KEY'),
                ]) {
                    sh """
                        cd infrastructure

                        export TF_VAR_image_tag=${GIT_COMMIT_HASH}
                        export TF_VAR_AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY_ID
                        export TF_VAR_AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_ACCESS_KEY

                        # Setup the remote backend to our Gitlab project
                        export GITLAB_ACCESS_TOKEN=\$TOKEN
                        terraform init \
                            -backend-config="address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp" \
                            -backend-config="lock_address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp/lock" \
                            -backend-config="unlock_address=https://gitlab.com/api/v4/projects/48388488/terraform/state/ae-capital-webapp/lock" \
                            -backend-config="username=beanah" \
                            -backend-config="password=\$GITLAB_ACCESS_TOKEN" \
                            -backend-config="lock_method=POST" \
                            -backend-config="unlock_method=DELETE" \
                            -backend-config="retry_wait_min=5"

                        # Apply the new image tag to the ECR task definition
                        terraform plan
                        terraform apply -auto-approve
                    """
                }
            }
        }        
    }
}