###
### ECR 
###
resource "aws_ecr_repository" "image_repo" {
  name                 = var.project
  image_tag_mutability = "MUTABLE"
  force_delete         = true
}

###
### ECS
###
resource "aws_ecs_task_definition" "task-def" {
  family                   = var.project
  network_mode             = "awsvpc"
  requires_compatibilities = ["EC2"]
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = aws_iam_role.tasks-service-role.arn

  container_definitions = jsonencode([
    {
      name        = "${var.project}-container"
      essential   = true
      image       = "${aws_ecr_repository.image_repo.repository_url}:${var.image_tag}"
      cpu         = "${tonumber(var.cpu)}"
      memory      = "${tonumber(var.memory)}"
      networkMode = "awsvpc"
      
      portMappings = [
        {
          containerPort = "${tonumber(var.port)}"
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "service" {
  name            = "${var.project}-service"
  cluster         = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.task-def.arn
  desired_count   = var.node_count
  launch_type     = "EC2"

  network_configuration {
    security_groups = [aws_security_group.task-sg.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.trgp.id
    container_name   = "${var.project}-container"
    container_port   = var.port
  }

  depends_on = [
    aws_alb_listener.alb-listener,
  ]
}