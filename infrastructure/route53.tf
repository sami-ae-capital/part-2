###
### Route53
###
data "aws_route53_zone" "root" {
  name = var.url
}

# Route53 Domains
resource "aws_route53_record" "a-record" {
  name    = var.url
  zone_id = data.aws_route53_zone.root.zone_id
  type    = "A"
  alias {

    # Traffic to ALB
    name                   = aws_alb.alb.dns_name
    zone_id                = aws_alb.alb.zone_id 
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "cname-wildcard" {
  name    = "*"
  zone_id = data.aws_route53_zone.root.zone_id
  type    = "CNAME"
  ttl     = 300

  weighted_routing_policy {
    weight = 10
  }  

  set_identifier = "*"
  records        = [var.url]
}

# Certificate
resource "aws_acm_certificate" "certificate" {
  domain_name               = var.url
  subject_alternative_names = [
    "*.${var.url}"
  ]
  validation_method         = "DNS"
  tags                      = {
    Name = "${var.project}-certificate"
  }
}

resource "aws_route53_record" "certvalidation" {
  name    =  tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_name
  records = [tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_value]
  type    = tolist(aws_acm_certificate.certificate.domain_validation_options)[0].resource_record_type

  allow_overwrite = true
  ttl             = 60
  zone_id         = data.aws_route53_zone.root.zone_id
}

resource "aws_acm_certificate_validation" "certvalidation" {
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [aws_route53_record.certvalidation.fqdn]
}