##
## ASG
##

# IAM for autoscaling
data "aws_iam_policy_document" "ecs-agent" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
} 

resource "aws_iam_role" "ecs-agent" {
  name               = "ecs-agent"
  assume_role_policy = data.aws_iam_policy_document.ecs-agent.json
}

resource "aws_iam_role_policy_attachment" "ecs-agent" {
  role       = aws_iam_role.ecs-agent.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs-agent" {
  name = "ecs-agent"
  role = aws_iam_role.ecs-agent.name
}

# Autoscaling group
resource "aws_autoscaling_group" "ecs_asg" {
    name                      = "${var.project}-asg"
    vpc_zone_identifier       = aws_subnet.private.*.id

    launch_template           {
      id = aws_launch_template.ecs.id
      version = "$Latest"      
    }

    desired_capacity          = 2
    min_size                  = 1
    max_size                  = 2
    health_check_grace_period = 300
    health_check_type         = "EC2"
}

# Launch template
resource "aws_launch_template" "ecs" {
  name_prefix   = "ecs-${var.project}-"
  image_id      = data.aws_ami.aws_optimized_ecs.id
  instance_type = var.instance_type

  iam_instance_profile {
    name = aws_iam_instance_profile.ecs-agent.name
  }

  user_data = base64encode("#!/bin/bash\necho ECS_CLUSTER=${var.project}-cluster >> /etc/ecs/ecs.config\necho ECS_ENABLE_CONTAINER_METADATA=true >> /etc/ecs/ecs.config")

  lifecycle {
    create_before_destroy = true
  }
}

# Get the latest AMI image
data "aws_ami" "aws_optimized_ecs" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn-ami*amazon-ecs-optimized"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["591542846629"] # AWS
}