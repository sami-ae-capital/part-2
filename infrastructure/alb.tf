###
### ALB
###
resource "aws_alb" "alb" {
  name            = "${var.project}-alb"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.alb-sg.id]
}

resource "aws_alb_listener" "alb-listener" {
  load_balancer_arn = aws_alb.alb.id
  port              = "443"
  protocol          = "HTTPS"

  ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
  certificate_arn   = aws_acm_certificate.certificate.arn

  default_action {
    target_group_arn = aws_alb_target_group.trgp.id
    type             = "forward"
  }
}

# HTTP redirect to HTTPS
resource "aws_lb_listener" "http-redirect" {
  load_balancer_arn = aws_alb.alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# Main target group
resource "aws_alb_target_group" "trgp" {
  name        = "${var.project}-trgp"
  port        = var.port
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    path      = "/"
  }
}

resource "aws_lb_listener_rule" "host-listener" {
  listener_arn = aws_alb_listener.alb-listener.arn
  priority     = var.port

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.trgp.arn
  }

  condition {
    host_header {
      values = [var.url] # Host based routing
    }
  }
}