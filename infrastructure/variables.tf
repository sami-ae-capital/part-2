# AWS
variable "AWS_ACCESS_KEY_ID" {
  sensitive   = true
  description = "AWS access key from project IAM user"
}

variable "AWS_SECRET_ACCESS_KEY" {
  sensitive   = true
  description = "AWS secret from project IAM user"
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "ap-southeast-2"
}

# Project and System 
variable "project" {
  description = "Name of the project as a whole."
  default     = "aecapital"
}

variable "url" {
  default     = ""
  description = "Application URL"
}

# Application
variable image_tag {
  description = "Image tag to use"
  type        = string
  default     = "latest"
}

variable port {
  description = "Application port"
  type        = number
  default     = 80
}

variable cpu {
  description = "CPU for application"
  type        = number
  default     = 256
}

variable memory {
  description = "Memory for application"
  type        = number
  default     = 512
}

variable node_count {
  description = "How many nodes"
  type        = number
  default     = 2
}

variable instance_type {
  description = "EC2 instance type to use"
  type        = string
  default     = "t2.small"
}

# AWS
variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default     = "172.17.0.0/16"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

# ECS
variable "ecs-task-service-role" {
  description = "Name of the stack"
}